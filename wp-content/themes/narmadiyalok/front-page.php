<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<!-- banner-sectoin -->
<section>
	<div class="banner" style="background-image: url('<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url(); } ?>');">
		<div class="inner-content">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
</section>
<!-- Ending banner-sectoin -->
<section class="content-sec padding-sm">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="siedbar-left box-shadow">
					<h4>
						Conservator Trustee
					</h4>
					<ul class="box-overflow">
						<?php
						$args = array( 'post_type' => 'conservator_trustee', 'posts_per_page' => -1 );
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post(); ?>
							<li>
								<div class="media">
									<div class="round-icon align-self-center"><i class="icon-man-user"></i></div>
									<div class="media-body">
										<span><?php the_title(); ?></span>
										<a href="tel:<?php the_field('contact_no'); ?>"><?php the_field('contact_no'); ?></a>      
									</div>
								</div>
							</li>
						<?php endwhile; ?>
					</ul>
				</div>
				<div class="siedbar-left box-shadow">
					<h4>
						Trustee
					</h4>
					<ul class="box-overflow">
						<?php
						$args = array( 'post_type' => 'trustee_post', 'posts_per_page' => -1 );
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post(); ?>
							<li>
								<div class="media">
									<div class="round-icon align-self-center">
										<i class="icon-man-user"></i></div>
										<div class="media-body">
											<span><?php the_title(); ?></span>
											<p><em><?php the_field('location'); ?></em></p>      
										</div>
									</div>
								</li>
							<?php endwhile; ?>
						</ul>
					</div>
				</div>	
				<div class="col-lg-6 padding-none">
					<div class="content">
						<?php while ( have_posts() ) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile;?>
					</div>
					<div class="block-content">
						<h3>लोक समाचार</h3>
						<?php
						$args = array( 'post_type' => 'post', 'posts_per_page' => 2 );
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post(); ?>
							<div class="row">
								<div class="col-sm-6">
									<h5><?php the_title(); ?></h5>
									<p><?php the_excerpt(); ?></p>
									<a href="<?php echo get_permalink(); ?>">पढ़ना जारी रखें<i class="icon-right-arrow"></i></a>
								</div>
								<div class="col-sm-6">
									<div class="thumb" style="background-image: url('<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url(); } ?>');"></div>
								</div>
							</div>
							<hr>
						<?php endwhile; ?>
					</div>
				</div>	
				<div class="col-lg-3">
					<div class="siedbar-left box-shadow">
						<h4>
							समाचार अद्यतन
						</h4>
						<ul class="box-overflow">
							<?php $the_query = new WP_Query( 'posts_per_page=5' ); ?>
							<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
								<li>
									<div class="media post">
										<div class="date-outer">
											<span><?php echo get_the_date('M'); ?></span>
											<p><?php echo get_the_date('d'); ?></p>
										</div>
										<div class="media-body">
											<p><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></p>     
											<span><?php the_excerpt(__('(more…)')); ?></span>
										</div>
									</div>
								</li>
								<?php
							endwhile;
							wp_reset_postdata();
							?>
						</ul>
					</div>
					<div class="advertisement">
						<?php
						$args = array( 'post_type' => 'advertisement', 'posts_per_page' => -1 );
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post(); ?>
							<div class="adve_img">
								<a href="<?php the_field('advertisement_url'); ?>" target="_blank">
									<img src="<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url(); } ?>"></a>
								</div>
							<?php endwhile; ?>
						</div>
					</div>	
				</div>
			</div>
		</section>
		<?php get_footer();
