<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package narmadiyalok
 */

get_header();
?>

	<section class="error-page">
		<div class="container">
			<div class="row">
				<div class="message-box">
				<h1>404</h1>
				<h2>Oops!Page Note Found</h2>
				<p>The page you werw looking for could not be found.</p>
				<div class="btn-outer">
					<a href="<?php echo esc_url(home_url('/')); ?>" class="btn-submit">Back To Home</a>
				</div>
			</div>
			</div>
		</div>
	</section>
<?php
get_footer();
