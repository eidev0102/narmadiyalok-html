<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package narmadiyalok
 */

?>
<div class="push"></div>
</div>
<footer>
	<div class="container text-center">
		<div class="row">
			<div class="col-sm-12">
				<div class="footer-menu">
					<?php
					wp_nav_menu(array(
						'theme_location' => 'menu-1',
						'container_class' => 'menu-outer',
						'container_id' => 'navbar',
						'menu_class' => 'menu',
					));
					?>
				</div>
				<div class="copyright">
					<p>Copyright @<?php echo date('Y'); ?> All right Reserved.</p>
				</div>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
