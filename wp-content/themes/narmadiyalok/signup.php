<?php
/**
 * The template for displaying all pages
 *Template Name: 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package narmadiyalok
 */

get_header();
?>
<section class="content-sec padding-sm">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="siedbar-left box-shadow">
					<h4>
						Conservator Trustee
					</h4>
					<ul class="box-overflow">
						<li>
							<div class="media">
								<div class="round-icon align-self-center"></div>
								<div class="media-body">
									<span>श्री काशीनाथ उपाध्याय</span>
									<p>+91-9425004116</p>      
								</div>
							</div>
						</li>
						<li>
							<div class="media">
								<div class="round-icon align-self-center"></div>
								<div class="media-body">
									<span>श्री काशीनाथ उपाध्याय</span>
									<p>+91-9425004116</p>      
								</div>
							</div>
						</li>
						<li>
							<div class="media">
								<div class="round-icon align-self-center"></div>
								<div class="media-body">
									<span>श्री काशीनाथ उपाध्याय</span>
									<p>+91-9425004116</p>      
								</div>
							</div>
						</li>
						<li>
							<div class="media">
								<div class="round-icon align-self-center"></div>
								<div class="media-body">
									<span>श्री काशीनाथ उपाध्याय</span>
									<p>+91-9425004116</p>      
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="siedbar-left box-shadow">
					<h4>
						Trustee
					</h4>
					<ul class="box-overflow">
						<li>
							<div class="media">
								<div class="round-icon align-self-center"></div>
								<div class="media-body">
									<span>श्री प्रेम लाल केसर</span>
									<p><em>इंदौर</em></p>      
								</div>
							</div>
						</li>
						<li>
							<div class="media">
								<div class="round-icon align-self-center"></div>
								<div class="media-body">
									<span>श्री प्रेम लाल केसर</span>
									<p><em>इंदौर</em></p>      
								</div>
							</div>
						</li>
						<li>
							<div class="media">
								<div class="round-icon align-self-center"></div>
								<div class="media-body">
									<span>श्री प्रेम लाल केसर</span>
									<p><em>इंदौर</em></p>      
								</div>
							</div>
						</li>
						<li>
							<div class="media">
								<div class="round-icon align-self-center"></div>
								<div class="media-body">
									<span>श्री प्रेम लाल केसर</span>
									<p><em>इंदौर</em></p>      
								</div>
							</div>
						</li>
						<li>
							<div class="media">
								<div class="round-icon align-self-center"></div>
								<div class="media-body">
									<span>श्री प्रेम लाल केसर</span>
									<p><em>इंदौर</em></p>      
								</div>
							</div>
						</li>
						<li>
							<div class="media">
								<div class="round-icon align-self-center"></div>
								<div class="media-body">
									<span>श्री प्रेम लाल केसर</span>
									<p><em>इंदौर</em></p>      
								</div>
							</div>
						</li>
						<li>
							<div class="media">
								<div class="round-icon align-self-center"></div>
								<div class="media-body">
									<span>श्री प्रेम लाल केसर</span>
									<p><em>इंदौर</em></p>      
								</div>
							</div>
						</li>
						
					</ul>
				</div>
			</div>	
			<div class="col-md-9 padding-none">
				<div class="content">
					<h3>नर्मदाय लोक सेवा में आपका स्वागत है</h3>
					<p>Lorem Ipsum प्रिंटिंग और टाइपसेटिंग उद्योग के बस डमी पाठ है। Lorem Ipsum 1500 के दशक के बाद से उद्योग के मानक डमी पाठ रहा है, जब एक अज्ञात प्रिंटर ने एक प्रकार की गैली ली और इसे एक प्रकार की नमूना किताब बनाने के लिए स्कैम्बल किया। यह न केवल पांच शताब्दियों तक जीवित रहा है, बल्कि इलेक्ट्रॉनिक टाइपसेटिंग में भी छलांग है, जो अनिवार्य रूप से अपरिवर्तित बनी हुई है।</p>
				</div>
				<div class="contant-form">
					<h2 class="heading">Contact Us</h2>
					<div class="form-outer">
						<form>
							<div class="row">
								<div class="col-sm-6">
									<div class="field-outer">
										<input type="text" name="firstname" class="form-control" placeholder="First Name">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="field-outer">
										<input type="text" name="lasttname" class="form-control" placeholder="Last Name">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="field-outer">
										<input type="emal" name="Email" class="form-control" placeholder="Email">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="field-outer">
										<input type="text" name="phonrno" class="form-control" placeholder="Phone No.">
									</div>
								</div>
							</div>
								<div class="row">
								<div class="col-sm-12">
									<div class="field-outer">
										<textarea class="form-control" placeholder="Message"></textarea>
									</div>
								</div>
							</div>
							<div class="btn-outer">
								<input type="submit" name="submit" class="btn-submit">
							</div>
						</form>
					</div>
				</div>
			</div>	
		</div>
	</div>
</section>

<?php
get_footer();
