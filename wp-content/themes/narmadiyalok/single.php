<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package narmadiyalok
 */

get_header();
?>
<!-- banner-sectoin -->
<section>
	<div class="banner">
		<div class="inner-content">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
</section>
<!-- Ending banner-sectoin -->

<section class="single-page content-padding bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<div class="left" style="background-image: url('<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url(); } ?>');">
				</div>
			</div>
			<div class="col-md-7">
				<div class="content">
					<h2><?php the_title(); ?></h2>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile;?>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="single-page content-padding text-left">
	<div class="container">
		<h2 class="heading">Other Posts</h2>
		<div class="row">
			<?php
			$currentID = get_the_ID();
			$args = array( 'post_type' => 'post', 'posts_per_page' => 3, 'post__not_in' => array($currentID) );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<div class="col-md-4">
					<div class="item box-shadow">
						<div class="thumb-outer">
						<div class="thumb" style="background-image: url('<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url(); } ?>');">
							<a href="<?php echo get_permalink(); ?>" class="link"></a>
						</div>
						</div>
						<a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
					</div>
				</div>
			<?php endwhile;  wp_reset_postdata(); ?>
		</div>
	</div>
</section>
<?php
get_footer();
