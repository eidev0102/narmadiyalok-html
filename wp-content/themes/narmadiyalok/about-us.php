<?php
/**
 * The template for displaying all pages
 *Template Name: About Us
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package narmadiyalok
 */

get_header();
?>
<!-- banner-sectoin -->
<section>
	<div class="banner" style="background-image: url('<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url(); } ?>');">
		<div class="inner-content">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
</section>
<!-- Ending banner-sectoin -->
<section class="content-sec padding-sm">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="siedbar-left box-shadow">
					<h4>
						Conservator Trustee
					</h4>
					<ul class="box-overflow">
						<?php
						$args = array( 'post_type' => 'conservator_trustee', 'posts_per_page' => -1 );
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post(); ?>
							<li>
								<div class="media">
									<div class="round-icon align-self-center"><i class="icon-man-user"></i></div>
									<div class="media-body">
										<span><?php the_title(); ?></span>
										<a href="tel:<?php the_field('contact_no'); ?>"><?php the_field('contact_no'); ?></a>      
									</div>
								</div>
							</li>
						<?php endwhile; ?>
					</ul>
				</div>
				<div class="siedbar-left box-shadow">
					<h4>
						Trustee
					</h4>
					<ul class="box-overflow">
						<?php
						$args = array( 'post_type' => 'trustee_post', 'posts_per_page' => -1 );
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post(); ?>
							<li>
								<div class="media">
									<div class="round-icon align-self-center">
										<i class="icon-man-user"></i></div>
										<div class="media-body">
											<span><?php the_title(); ?></span>
											<p><em><?php the_field('location'); ?></em></p>      
										</div>
									</div>
								</li>
							<?php endwhile; ?>
						</ul>
					</div>
				</div>	
				<div class="col-lg-9 padding-none">
					<div class="content">
						<?php while ( have_posts() ) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile;?>
					</div>
				</div>	
			</div>
		</div>
	</section>

	<?php
	get_footer();
