<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package narmadiyalok
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/assets/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<header>
		<div class="strip">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-6">
						<ul>
							<?php if(get_field('phone_no', 'options')): ?>
								<li><a href="tel:<?php the_field('phone_no', 'options'); ?>"><i class="icon-telephone-handle-silhouette"></i><?php the_field('phone_no', 'options'); ?></a></li>
							<?php endif; ?>
							<?php if(get_field('email', 'options')): ?>
								<li><a href="mailto:<?php the_field('email', 'options'); ?>"><i class="icon-envelope"></i><?php the_field('email', 'options'); ?></a></li>
							<?php endif; ?>
						</ul>
					</div>
					<div class="col-md-6 col-sm-6 col-6">
						<div class="content">
							<?php if ( is_user_logged_in() ) { ?>
								<p>For Log out Click Here</p>
								<a href="<?php echo esc_url(home_url('/')); ?>logout" class="sm-btn">Logout</a>
							<?php } else{ ?>
								<a href="<?php echo esc_url(home_url('/')); ?>register" class="link">Not a member?</a>
								<a href="<?php echo esc_url(home_url('/')); ?>login" class="sm-btn">Login</a>
							<?php }?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-expand-md navbar-light">
			<div class="container">
				<?php if(get_field('site_logo', 'options')): ?>
					<a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php the_field('site_logo', 'options'); ?>"></a>
				<?php endif; ?>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" id="nav-icon1">
					<span class="icon-bar top-bar"></span>
					<span class="icon-bar middle-bar"></span>
					<span class="icon-bar bottom-bar"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<?php
					wp_nav_menu(array(
						'theme_location' => 'menu-1',
						'container_class' => 'menu-outer',
						'container_id' => 'navbar',
						'menu_class' => 'menu',
					));
					?>
				</div>
			</div>
		</nav>
	</header>
	<div class="wrapper">